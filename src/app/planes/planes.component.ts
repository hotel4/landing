import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ReservaService } from '../servicio/reserva.service';

@Component({
  selector: 'app-planes',
  templateUrl: './planes.component.html',
  styleUrls: ['./planes.component.css']
})
export class PlanesComponent implements OnInit {

  planes:Array<any>=[]
  constructor(private _proxy: HttpClient, public _router: Router, public _reserva:ReservaService) { }

  ngOnInit(): void {
    this._proxy.get("http://localhost:55909/api/Values").subscribe((datos:Array<any>)=>{
      console.log(datos)
      this.planes=datos
    })
  }

  reservarHabitacion(habitacion){
    console.log(habitacion)
    this._reserva.habitacion=habitacion
    this._router.navigate(["/reserva"])
  }
}
