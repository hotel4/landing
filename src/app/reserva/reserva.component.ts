import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { ReservaService } from '../servicio/reserva.service';

@Component({
  selector: 'app-reserva',
  templateUrl: './reserva.component.html',
  styleUrls: ['./reserva.component.css']
})
export class ReservaComponent implements OnInit {
  nombre:string
  correo:string
  telefono:string
  apellido:string
  pais:string
  ciudad:string
  fsalida:Date
  fingreso:Date
  comentario:string
  @ViewChild('mensajeGuardado') private mensajeGuardado: SwalComponent;
  constructor(private _proxy: HttpClient, public _reserva:ReservaService, public _router: Router,) { }
  guardarReserva(){
    this._proxy.post("http://localhost:55909/api/Reserva",{
      "id_habitacion": this._reserva.habitacion.id,
      "fecha_entrada": this.fingreso,
      "fecha_salida": this.fsalida,
      "ocupacion_adulto": this._reserva.habitacion.Adultos,
      "ocupacion_nino": this._reserva.habitacion.Ninos,
      "precio_final": this._reserva.habitacion.precio,
      "usuario": {
        "nombre": this.nombre,
        "apellido": this.apellido,
        "correo": this.correo,
        "pais": this.pais,
        "telefono": this.telefono,
        "ciudad": this.ciudad,
        "comentario": this.comentario,
        "id_reservas": 1
      }
    }).subscribe((datos)=>{
      this.mensajeGuardado.update({
        text:"Reserva guardada con exito",
        onAfterClose:()=>{
          this._router.navigate(["/"])
        },
        showConfirmButton:true
      })
      this.mensajeGuardado.fire()
    })
  }

  ngOnInit(): void {
  }
  
}
